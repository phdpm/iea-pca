# lazy load function to load or install package
lazy <- function (pkg) {
  if (is.vector(pkg) & length(pkg) > 1) {
    for (p in pkg) {
      if (!require(p, character.only = T))
        install.packages(p, character.only = T)
    }
  } else {
    if (!require(pkg))
      install.packages(pkg)
  }
}

# load or install
lazy(c("factoextra", "gridExtra", "ggplot2", "reshape2", "rmarkdown", "dplyr", "foreach", "corrplot", "explor"))