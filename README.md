# Интегрированный экосистемный анализ методом PCA

> English description available below

Данный репозиторий содержит реализацию экосистемного анализа, выполненного на основе метода главных компонент, PCA. Теоретический смысл и пояснения к данному методу здесь даны не будут. 

В репозитории содержится весь необходимый программный код для выполнения полноценного анализа и автоматической генерации всех графиков, гистограмм и 3D модели пространства векторов.

Алгоритм существенно доработан для описания экосистемных связей а так же обнаружения экосистемных сдвигов (regime shift) при помощи нескольких методов: иерархическая кластерилация, метод Rodionov regime shift detection, проекция координатных состояний PCA.

## 1. Входные данные
Входные данные необходимо расположить в файле `/input/data.csv` в формате `csv`. Разделитель десятичной части - точка. Если в данных имеются пропуски - заполнить их при помощи геометрического среднего из 2 - 4 соседних лет наблюдений.

## 2. Установка зависимостей
После подготовки входных данных необходимо выполнить установку всех пакетов, используемых для анализа:

```
source("install.R")
```

## 3. Выполнение анализа
Перед выполнением анализа необходимо выполнить параметризацию алгоритма поиска экосистемных сдвигов и другие параметры в файле `run.R`:

```
config.rodionov.L <- 7 # rodionov regime-shift minimum length (7 = regime shift can happens not ofter then 1 time in 7 years)
config.rodionov.alpha <- 0.05 # confidence level, alpha = 0.05 equals p = 0.95
```


Для выполнения всей процедуры анализа необходимо выполнить:

```
source("run.R")
```

Название и заголовки отчета могут быть отредактированы в файле `Report_col.Rmd` и `Report_bw.Rmd`.

## 4. Результаты анализа
Результаты моделирования будут сохранены в два отчета: `Report_col.html` и `Report_bw.html`. 

## 5. Автор
Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)

# Integrated ecosystem analysis by PCA

This repository provide full-done ecosystem assessment scripts done by principle component analysis (PCA). Theoretical meanings are not reviewed there.

The default PCA algorithm with some custom modification used in R, method `prcomp()` with full done plots, hists & 3d dimension model. Regime shift detection by Rodionov and hierarchical clustering implemented.

## 1. Input data
Put your input data to `/input/data.csv`. If data contains `NA's` - use geometrical mean approximation to feel it up.

## 2. Install required packages
After input data preparation - install required packages by script:
```
source("install.R")
```

## 3. Perform IEA
Before performing analysis you should done configuration in head of `run.R`:
```
config.rodionov.L <- 7 # rodionov regime-shift minimum length (7 = regime shift can happens not ofter then 1 time in 7 years)
config.rodionov.alpha <- 0.05 # confidence level, alpha = 0.05 equals p = 0.95
```

and then run:
``` 
source("run.R")
```

## 4. Report results
After script is done you will find generated reports `Report_col.Rmd` and `Report_bw.Rmd` with full results & graphs.

## 5. Author

Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)